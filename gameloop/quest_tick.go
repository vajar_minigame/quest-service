package gameservices

import (
	"context"

	"gitlab.com/vajar_minigame/proto_go/gen/common"
	"gitlab.com/vajar_minigame/proto_go/gen/quest"

	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/quest-service/data"
)

// loop over all quests and update them
//todo some form of batching so that it doesnt block for that long
func (g *game) questTick() {

	mutex := g.questDB.GetQuestLock()
	mutex.Lock()
	defer mutex.Unlock()

	activeQuests := g.questDB.GetAllActiveQuests()
	quests := g.questDB.GetAllQuests()
	for i, aQuest := range activeQuests {
		quest := quests[i]

		aQ, done := updateQuest(quest, aQuest)
		if done == false {
			activeQuests[i] = aQ
			continue

		}

		mutex.Unlock()
		// quest is done handle this case
		err := g.questService.MarkQuestComplete(context.Background(), &common.QuestId{Id: quest.ID})
		if err != nil {
			logrus.Errorf("%v", err)
		}
		mutex.Lock()
	}

}

func updateQuest(currQuest data.QuestComp, activeQ *quest.ActiveQuest) (*quest.ActiveQuest, bool) {
	t, err := ptypes.Timestamp(activeQ.StartTime)
	if err != nil {
		logrus.Errorf("could not conver time for %v", activeQ)
	}
	deltaTime := time.Since(t)

	if deltaTime > time.Duration(currQuest.Duration)*time.Second {
		return &quest.ActiveQuest{}, true
	}

	return activeQ, false

}
