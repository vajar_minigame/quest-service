package data

type QuestComp struct {
	ID                  int32
	PlayerID            int32
	Difficulty          int32
	RecommendedStrength int32
	Duration            int32
}

func NewQuestComp(ID int32, PlayerID int32, Difficulty int32, RecommendedStrength int32, Duration int32) QuestComp {
	return QuestComp{ID: ID, PlayerID: PlayerID, Difficulty: Difficulty, RecommendedStrength: RecommendedStrength, Duration: Duration}
}
