package router

import (
	"log"
	"net"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/vajar_minigame/minigame_backend/clientsession"
	"gitlab.com/vajar_minigame/proto_go/gen/interaction"
	"gitlab.com/vajar_minigame/proto_go/gen/item"
	"gitlab.com/vajar_minigame/proto_go/gen/monster"
	"gitlab.com/vajar_minigame/proto_go/gen/quest"
	"gitlab.com/vajar_minigame/proto_go/gen/user"

	"gitlab.com/vajar_minigame/minigame_backend/controller"

	"gitlab.com/vajar_minigame/proto_go/gen/auth"
	"google.golang.org/grpc"

	"gitlab.com/vajar_minigame/minigame_backend/gameservices"

	"gitlab.com/vajar_minigame/minigame_backend/storage"
	"gitlab.com/vajar_minigame/minigame_backend/storage/mock"

	"github.com/sirupsen/logrus"
	"gitlab.com/vajar_minigame/minigame_backend/config"

	"go.opentelemetry.io/otel/api/global"
	"go.opentelemetry.io/otel/plugin/grpctrace"
)

type server struct {
	db     dbs
	config *config.Config
}

type dbs struct {
	storage.QuestDB
}

const address = ":8080"

func startNatsPublisher(config *config.Config) clientsession.NatsPublisher {

	nats, err := clientsession.NewNatsPublisher(config)
	if err != nil {
		logrus.Fatal("cant connect to nats")
	}
	return nats
}

func startGRPC(config *config.Config, userDB storage.UserDB, invService *controller.InventoryService, monService *controller.MonService, questService *controller.QuestService, interactionService interaction.InteractionServiceServer, nats clientsession.NatsPublisher) {
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	userService := controller.NewUserService(config, userDB, nats)
	authService, _ := controller.NewAuthService(userDB, config)
	//grpc.StreamInterceptor(grpc_auth.StreamServerInterceptor(authService.)

	grpcServer := grpc.NewServer(grpc.UnaryInterceptor(grpctrace.UnaryServerInterceptor(global.Tracer(""))),
		grpc.StreamInterceptor(grpctrace.StreamServerInterceptor(global.Tracer(""))))

	auth.RegisterAuthServer(grpcServer, authService)
	user.RegisterUserControllerServer(grpcServer, userService)
	item.RegisterItemServiceServer(grpcServer, invService)
	monster.RegisterMonsterServicesServer(grpcServer, monService)
	interaction.RegisterInteractionServiceServer(grpcServer, interactionService)
	quest.RegisterQuestServicesServer(grpcServer, questService)
	err = grpcServer.Serve(lis)
	if err != nil {
		logrus.Fatal(err)
	}
}

func StartServer() {
	config := config.LoadConfig("resources/config")

	logrus.SetLevel(logrus.TraceLevel)
	logrus.SetReportCaller(true)

	userDB, monDBIntern, itemDB, questDB := mock.NewDatabases(config)

	monDB := monDBIntern.ConvertToMonsterDB()
	storage.BootstrapDBs(userDB, monDB, itemDB, questDB)

	logrus.Info("Connection to DB established")

	nats := startNatsPublisher(config)

	dbs := controller.NewDBs(userDB, monDB, itemDB)
	monService := controller.NewMonService(config, monDB, nats)
	invService := controller.NewInvService(config, itemDB, nats)
	questService := controller.NewQuestService(config, monService, questDB, nats)
	gameServices := gameservices.NewGameServices(monDBIntern, itemDB, questDB, questService)

	interactionService := controller.NewInteractionService(config, invService, monService)

	//newController := controller.NewController(monService, invService, interactionService, questService, userService)

	go startGRPC(config, dbs.UserDB, invService, monService, questService, interactionService, nats)
	logrus.Debug("connecting to nats")

	go gameServices.Run()

	//go newController.Listen()

	r := mux.NewRouter()
	//r.HandleFunc("/ws", wss.NewWebsocketConn)

	logrus.Fatal(http.ListenAndServe(":4500", handlers.CORS()(r)))

}
