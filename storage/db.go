type QuestDB interface {
	AddQuest(ctx context.Context, quest *quest.Quest) (*common.QuestId, error)
	UpdateQuest(ctx context.Context, quest *quest.Quest) error
	GetQuest(ctx context.Context, id *common.QuestId) (*quest.Quest, error)
	StartQuest(ctx context.Context, id *common.QuestId, mons []*common.MonId) error
	GetQuestsByUser(ctx context.Context, userID *common.UserId) ([]*quest.Quest, error)
	DeleteQuest(ctx context.Context, id *common.QuestId) error

	GetQuestComps() map[int32]data.QuestComp
}